package broker;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import datamodel.Location;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

/**
 * Broker pulls data out of the queue and sends them to the server using the
 * REST service that the server made available.
 *
 * @author Guus Klinkenberg & Laura Baakman.
 */
public class Broker {

	private QueueingConsumer consumer;
	private WebResource service;
	private final static String QUEUE_NAME = "GPSLocation";
	private final static String SERVER_ADDRESS = "localhost";
	private final static String SERVER_PORT = "8080";

	private BasicDataSource connectionPool;

	private boolean storeInDatabase;

	public void setupDatabase() {
		Properties props = new Properties();
		FileInputStream fis = null;
		java.sql.Connection con = null;
		try {
			fis = new FileInputStream("db.properties");
			props.load(fis);

			storeInDatabase = Boolean.parseBoolean(props.getProperty("STORE_IN_DB"));

			// load the Driver Class
			Class.forName(props.getProperty("DB_DRIVER_CLASS"));

			// setup the pool
			connectionPool = new BasicDataSource();
			connectionPool.setUrl(props.getProperty("DB_URL"));
			System.out.println(props.getProperty("DB_URL"));
			connectionPool.setUsername(props.getProperty("DB_USERNAME"));
			connectionPool.setPassword(props.getProperty("DB_PASSWORD"));

			// create the connection now
//            con = DriverManager.getConnection(props.getProperty("DB_URL"),
//                    props.getProperty("DB_USERNAME"),
//                    props.getProperty("DB_PASSWORD"));
		} catch (IOException ex) {
			Logger.getLogger(Broker.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(Broker.class.getName()).log(Level.SEVERE, null, ex);
		}
//        return con;
	}

	public Broker() {

		try {
			// Init RabbitMQ
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(SERVER_ADDRESS);
			Connection connection;
			Channel channel;

			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
			consumer = new QueueingConsumer(channel);
			channel.basicConsume(QUEUE_NAME, true, consumer);

			// Init connection pool using dbcp
			setupDatabase();
			System.out.println("Will store in database: " + storeInDatabase);
		} catch (IOException ex) {
			System.err.println("Couldn't establish connection with queue");
			System.exit(-1);
		}

		//Init connection with webservice
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		service = client.resource(getBaseURI());
	}

	private void storeLocation(Location toStore) {
		java.sql.Connection conn;
		try {
			conn = connectionPool.getConnection();
			Statement stmnt = conn.createStatement();

			stmnt.executeUpdate(
					"INSERT INTO netcomputing.geodata (id, time, longx, laty) VALUES ("
					+ toStore.getId()
					+ ", NOW(), "
					+ toStore.getX() + ", "
					+ toStore.getY() + ");");
			stmnt.close();
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(Broker.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public void brokeMessages() {
		Location message;
		while (true) {
			QueueingConsumer.Delivery delivery;
			try {
				//Get message from RabbitMQ
				delivery = consumer.nextDelivery();
				message = new Location(delivery.getBody());
				System.out.println(" [x] Received '" + message + "'");

				// Send data to REST service
				ClientResponse response = service.path("rest").path("locations")
						.accept(MediaType.APPLICATION_XML)
						.put(ClientResponse.class, message);

				// Put data in DB
				if (storeInDatabase) {
					storeLocation(message);
				}

			} catch (ConsumerCancelledException ex) {
				//Missed one delivery, shouldn't be problematic
				ex.printStackTrace();
			} catch (ShutdownSignalException ex) {
				//Missed one delivery, shouldn't be problematic
				ex.printStackTrace();
			} catch (ClientHandlerException ex) {
				//Missed one delivery, shouldn't be problematic
				ex.printStackTrace();
			} catch (UniformInterfaceException ex) {
				//Missed one delivery, shouldn't be problematic
				ex.printStackTrace();
			} catch (InterruptedException ex) {
				//Missed one delivery, shouldn't be problematic
				ex.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Broker broker = new Broker();
		broker.brokeMessages();
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://" + SERVER_ADDRESS + ":" + SERVER_PORT + "/netcomputing/").build();
	}
}
