package datamodel;

import java.awt.Point;
import java.nio.ByteBuffer;
import java.util.UUID;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Storage object that stores the x and y coordinates of a client that is
 * identified with the (also) stored id.
 *
 * @author Guus Klinkenberg & Laura Baakman
 */
@XmlRootElement
public class Location {

    private Integer id;
    private Double x;
    private Double y;

    /**
     * Empty constructor which is advised not to be used.
     *
     * This constructor is here to allow (un)marshalling using JAXB.
     */
    public Location() {
    }

    /**
     * Creates new Location with parameters
     *
     * @param id id of the client.
     * @param x x coordinate of location to store.
     * @param y y coordinate of location to store.
     */
    public Location(UUID id, Double x, Double y) {
        this(id.hashCode(), x, y);
    }

    /**
     * Creates new Location with parameters
     *
     * @param id id of the client.
     * @param point point from which the x and y coordinate will be stored in
     * the respective, right, variable.
     */
    public Location(UUID id, GPSPoint point) {
        this(id.hashCode(), point.x, point.y);
    }

    /**
     * Creates new Location with parameters
     *
     * @param id id of the client.
     * @param x x coordinate of location to store.
     * @param y y coordinate of location to store.
     */
    public Location(int id, Double x, Double y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    /**
     * Constructor that generates a Location from a byte array.
     *
     * This constructor is meant for RabbitMQ. Other uses are discouraged.
     * However, if one wants to use this, the order within the byte array should
     * be: x, y, id all as primary type integers.
     *
     * @param array a BigEndian byte array containing three ints
     */
    public Location(byte[] array) {
        ByteBuffer buf = ByteBuffer.wrap(array);
        this.x = buf.getDouble();
        this.y = buf.getDouble();
        this.id = buf.getInt();
    }

    /**
     * Order of the byte array is x, y, id.
     *
     * @return the byte array that represents this Location
     */
    public byte[] getBytes() {
        ByteBuffer dbuf = ByteBuffer.allocate(3 * (Double.SIZE / Byte.SIZE));
        dbuf.putDouble(this.x);
        dbuf.putDouble(this.y);
        dbuf.putDouble(this.id);
        return dbuf.array();
    }

    /**
     * @return id of Location as Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id of the location.
     *
     * @param id new id of the Location
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return id of Location as string
     */
    public String getIdAsString() {
        return id.toString();
    }

    /**
     * @return x of Location
     */
    public Double getX() {
        return x;
    }
	
	public Integer getIntX(){
		return x.intValue();
	}

	public Integer getIntY(){
		return y.intValue();
	}
	
    /**
     * Sets y of the location.
     *
     * @param x new coordinate of the Location
     */
    public void setX(Double x) {
        this.x = x;
    }

    /**
     * @return y of Location
     */
    public Double getY() {
        return y;
    }

    /**
     * Sets y of the location.
     *
     * @param y new coordinate of the Location
     */
    public void setY(Double y) {
        this.y = y;
    }

    /**
     * @return a string representation of this Location
     */
    @Override
    public String toString() {
        return this.id + " : (" + this.x + ", " + this.y + ")";
    }
}
